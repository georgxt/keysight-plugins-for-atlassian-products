package com.keysight.test.ecosystem.rest;

import java.util.Map;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.net.HttpURLConnection;
import java.net.URLDecoder;
import java.net.URLEncoder;

import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.confluence.plugin.services.VelocityHelperService;
import com.atlassian.confluence.setup.settings.SettingsManager;

@Path("/proxy/")
public class RestProxyService
{
   private final SettingsManager settingsManager;
   private final VelocityHelperService velocityHelperService;

   public RestProxyService( SettingsManager settingsManager,
                            VelocityHelperService velocityHelperService
   ){
       this.settingsManager            = settingsManager;
       this.velocityHelperService      = velocityHelperService;
   }

   @GET
   @Path("Get/{hostname}")
   @AnonymousAllowed
   @Produces({MediaType.APPLICATION_JSON})
   @Consumes({MediaType.APPLICATION_JSON})
   public Response proxyCallToETMSStation( @PathParam("hostname") String hostname,
                                           @QueryParam("keyValueCsvString") String keyValueCsvString ){
      String json = null;

      try{
         URL url = new URL( "http://" + hostname + ":3867/EtmsWebServices/rest/Get?keyValueCsvString="+URLEncoder.encode( keyValueCsvString ) );
         HttpURLConnection connection = (HttpURLConnection) url.openConnection();
         connection.setDoInput(true);
         connection.setRequestMethod("GET");
         connection.setRequestProperty("Content-Type", "application/json");
         connection.setRequestProperty("charset", "utf-8" );
         connection.connect();

         int statusCode = connection.getResponseCode();
         if( statusCode == 200 || statusCode == 201 ) {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            StringBuilder stringBuilder = new StringBuilder();
            String line;
            while ((line = bufferedReader.readLine()) != null) {
               stringBuilder.append(line + "\n");
            }
            bufferedReader.close();
            json = stringBuilder.toString();
         }
      }
      catch( Exception exception )
      {
      }

      return Response.ok( json ).build();
   }
}
