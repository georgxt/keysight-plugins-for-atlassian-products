<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
 
    <modelVersion>4.0.0</modelVersion>
    <groupId>com.keysight</groupId>
    <artifactId>parent</artifactId>
    <version>1.0.0</version>
 
    <organization>
        <name>Keysight Technologies Inc.</name>
        <url>http://www.keysight.com/</url>
    </organization>
  
    <name>Parent</name>
    <description>This is the parent pom for com.keysight plugins</description>
    <packaging>pom</packaging>
 
    <properties>
        <confluence.version>7.8.0</confluence.version>
        <!-- <confluence.data.version>LATEST</confluence.data.version> -->
        <confluence.data.version>7.8.0</confluence.data.version>

        <!-- use atlas-version to see what version of AMP the sdk provides -->
        <amps.version>8.0.2</amps.version>
        <plugin.testrunner.version>2.0.1</plugin.testrunner.version>
        <atlassian.spring.scanner.version>1.2.13</atlassian.spring.scanner.version>

        <maven.resources.plugin.version>2.7</maven.resources.plugin.version>
	    <maven.compiler.plugin.version>3.3</maven.compiler.plugin.version>
	    <create.content.version>6.0.19</create.content.version>
	    <confluence.space.ia.version>13.1.2</confluence.space.ia.version>
	    <spring.context.version>4.2.5.RELEASE</spring.context.version>

        <maven.compiler.source>1.8</maven.compiler.source>
        <maven.compiler.target>1.8</maven.compiler.target>
        <encoding>UTF-8</encoding>

        <!-- This key is used to keep the consistency between the key in atlassian-plugin.xml and the key to generate bundle. -->
        <atlassian.plugin.key>${project.groupId}.${project.artifactId}</atlassian.plugin.key>
    </properties>
	
	<repositories>
        <repository>
            <id>atlassian-public</id>
            <url>https://maven.atlassian.com/repository/public</url>
            <snapshots>
                <enabled>true</enabled>
                <updatePolicy>never</updatePolicy>
                <checksumPolicy>warn</checksumPolicy>
            </snapshots>
            <releases>
                <enabled>true</enabled>
                <checksumPolicy>warn</checksumPolicy>
            </releases>
        </repository>
	</repositories>

    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>com.atlassian.confluence</groupId>
                <artifactId>confluence-plugins-platform-pom</artifactId>
                <version>${confluence.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>
 
    <dependencies>
        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>4.12</version>
            <scope>test</scope>
        </dependency>
 
        <dependency>
            <groupId>com.atlassian.confluence</groupId>
            <artifactId>confluence</artifactId>
            <version>${confluence.version}</version>
            <scope>provided</scope>
            <exclusions>
                <exclusion>
                    <groupId>tangosol-coherence</groupId>
                    <artifactId>tangosol</artifactId>
                </exclusion>
                <exclusion>
                    <groupId>tangosol-coherence</groupId>
                    <artifactId>coherence</artifactId>
                </exclusion>
            </exclusions>
        </dependency>

        <dependency>
            <groupId>com.atlassian.plugins.rest</groupId>
            <artifactId>atlassian-rest-common</artifactId>
            <version>2.6.3.1</version>
            <scope>provided</scope>
        </dependency>
 
        <dependency>
            <groupId>com.atlassian.plugin</groupId>
            <artifactId>atlassian-spring-scanner-annotation</artifactId>
            <version>${atlassian.spring.scanner.version}</version>
            <scope>compile</scope>
        </dependency>

        <dependency>
            <groupId>com.atlassian.plugin</groupId>
            <artifactId>atlassian-spring-scanner-runtime</artifactId>
            <version>${atlassian.spring.scanner.version}</version>
            <scope>runtime</scope>
        </dependency>
 
        <dependency>
            <groupId>javax.inject</groupId>
            <artifactId>javax.inject</artifactId>
            <version>1</version>
            <scope>provided</scope>
        </dependency>

        <dependency>
           <groupId>com.atlassian.templaterenderer</groupId>
           <artifactId>atlassian-template-renderer-api</artifactId>
           <version>1.1.1</version>
           <scope>provided</scope>
        </dependency>

        <dependency>
            <groupId>javax.xml.bind</groupId>
            <artifactId>jaxb-api</artifactId>
            <version>2.1</version>
            <scope>provided</scope>
        </dependency>

        <dependency>
            <groupId>javax.servlet</groupId>
            <artifactId>javax.servlet-api</artifactId>
            <version>4.0.1</version>
            <scope>provided</scope>
        </dependency>

        <dependency>
            <groupId>com.atlassian.confluence.plugins</groupId>
            <artifactId>confluence-create-content-plugin</artifactId>
            <version>${create.content.version}</version>
            <scope>provided</scope>
        </dependency>

        <dependency>
            <groupId>com.atlassian.confluence.plugins</groupId>
            <artifactId>confluence-space-ia</artifactId>
	    <version>${confluence.space.ia.version}</version>
            <scope>provided</scope>
        </dependency>

        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-context</artifactId>
	    <version>${spring.context.version}</version>
            <scope>provided</scope>
        </dependency>

        <dependency>
           <groupId>com.atlassian.sal</groupId>
           <artifactId>sal-api</artifactId>
           <version>3.0.6</version>
           <scope>provided</scope>
        </dependency>
  
        <!-- WIRED TEST RUNNER DEPENDENCIES -->
        <dependency>
            <groupId>com.atlassian.plugins</groupId>
            <artifactId>atlassian-plugins-osgi-testrunner</artifactId>
            <version>${plugin.testrunner.version}</version>
            <scope>test</scope>
        </dependency>
 
        <dependency>
            <groupId>javax.ws.rs</groupId>
            <artifactId>jsr311-api</artifactId>
            <version>1.1.1</version>
            <scope>provided</scope>
        </dependency>

        <dependency>
            <groupId>com.google.code.gson</groupId>
            <artifactId>gson</artifactId>
            <version>2.2.2-atlassian-1</version>
        </dependency>

    </dependencies>
 
    <build>
        <plugins>
            <plugin>
                <groupId>com.atlassian.maven.plugins</groupId>
                <artifactId>confluence-maven-plugin</artifactId>
                <version>${amps.version}</version>
                <extensions>true</extensions>
                <configuration>
                    <productVersion>${confluence.version}</productVersion>
                    <productDataVersion>${confluence.data.version}</productDataVersion>
                    <enableQuickReload>true</enableQuickReload>
                    <enableFastdev>false</enableFastdev>
		    <encoding>${encoding}</encoding>
		    <jvmArgs>-Xms128m -Xmx2048m -XX:MaxPermSize=512m</jvmArgs>
		    <jvmDebugPort>5005</jvmDebugPort>
                    <instructions>
                        <Atlassian-Plugin-Key>${atlassian.plugin.key}</Atlassian-Plugin-Key>
 
                        <!-- Add package to export here -->
                        <Export-Package>
                        </Export-Package>
 
                        <!-- Add package import here -->
                        <Import-Package>
                            org.springframework.osgi.*;resolution:="optional",
                            org.eclipse.gemini.blueprint.*;resolution:="optional",
                            *
                        </Import-Package>
 
                        <!-- Ensure plugin is spring powered - see https://extranet.atlassian.com/x/xBS9hQ  -->
                        <Spring-Context>*</Spring-Context>
                    </instructions>
                </configuration>
            </plugin>
 
            <plugin>
                <groupId>com.atlassian.plugin</groupId>
                <artifactId>atlassian-spring-scanner-maven-plugin</artifactId>
                <version>${atlassian.spring.scanner.version}</version>
                <executions>
                    <execution>
                        <goals>
                            <goal>atlassian-spring-scanner</goal>
                        </goals>
                        <phase>process-classes</phase>
                    </execution>
                </executions>
                <configuration>
                    <scannedDependencies>
                        <dependency>
                            <groupId>com.atlassian.plugin</groupId>
                            <artifactId>atlassian-spring-scanner-external-jar</artifactId>
                        </dependency>
                    </scannedDependencies>
                    <verbose>false</verbose>
                </configuration>
            </plugin>
 
            <plugin>
                <artifactId>maven-resources-plugin</artifactId>
                <version>${maven.resources.plugin.version}</version>
                <configuration>
                    <encoding>${encoding}</encoding>
                </configuration>
            </plugin>

            <!--
            <plugin>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>${maven.compiler.plugin.version}</version>
                <configuration>
                    <encoding>${encoding}</encoding>
                </configuration>
            </plugin>
            -->
        </plugins>
    </build>

  <modules>
    <module>confluence-support</module>
    <module>database</module>
    <module>glossary</module>
    <module>guidance</module>
    <module>header-menus</module>
    <module>html-elements</module>
    <module>include-content</module>
    <module>keysight-theme</module>
    <module>learning-products</module>
    <module>mathjax</module>
    <module>test-ecosystem</module>
  </modules>
</project>
