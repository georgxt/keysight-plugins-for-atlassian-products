#set( $images = "$webResourceManager.getStaticResourcePrefix()/download/resources/com.keysight.header-menus:header-menus-admin-config-resources/images" )

<html>
    <head>
        <title>Header Menus Configuration</title>
        <meta name="decorator" content="atl.admin"/>
        $webResourceManager.requireResource("com.keysight.header-menus:header-menus-admin-config-resources")
    </head>
    <body>
        #if( $tryToSaveConfigXml )
            <div class="try-to-save-config-xml">
            #if( $savedConfigXml )
               <div class="successfully-saved-config-xml">
               <script type="text/javascript">
                  //<![CDATA[
                  AJS.flag({
                    type: 'success',
                    title: 'Saved',
                    body: 'Saved the config.',
                    close: 'auto'
                  });
                  //]]>
               </script>
               </div>
            #else
               <div class="failed-to-save-config-xml">
               <script type="text/javascript">
                  //<![CDATA[
                  AJS.flag({
                    type: 'error',
                    title: 'Error',
                    body: 'Failed to save the config.',
                    close: 'auto'
                  });
                  //]]>
               </script>
               </div>
            #end
            </div>
        #end

        <h2>Introduction</h2>
        <p>The Keysight Headers Menu plugin provides the ability to create up to five menu items in the
        top confluence header.  For each of the configurations below, if there is no label, no item
        will be placed into the header menu.  If there is nothing in the <strong>popupmenu</strong>
        text area, then clicking on the menu item will take you to the specified <strong>url</strong>.
        If there is something in the <strong>popupmenu</strong> text area, then the <strong>url</strong>
        will be ignored as the popup menu will be shown when the item is clicked.</p>

        <h3>Creating a Dropdown menu</h3>
        <p>The drop down menu for each of the header bar is created by entering text into the <strong>popupmenu</strong>
        text area.  A link is represented by a label, a comma, and the associated url.</p>

        <pre>Google,https://www.google.com</pre>

        <p>If you add a line without a comma, it is treated as a section heading.  If you add a
        line with one or more dashes, &quot;-&quot;, that will start a new section that effectively
        adds a vertical line above the entry.<p>

        <pre>
        Search Engines
        Google,https://www.google.com
        Bing,https://www.bing.com
        Social Media
        Facebook,https://facebook.com
        Twitter,https://www.twitter.com
        -----
        YouTube,https://youtube.com
        </pre>

        <form id="header-menus-admin-config" class="aui">

            #set($menus = ["A", "B", "C", "D", "E"])
            #foreach( $menuLetter in $menus )
               <h2>Menu $menuLetter</h2>
               <div class="field-group">
                   <label for="menu-${menuLetter}-label">Label</label>
                   <input class="text long-field" type="text" id="menu-${menuLetter}-label"
                          name="menu-${menuLetter}-label" title="Label ${menuLetter} Menu"/>
                   <div class="error menu-${menuLetter}-label-error"></div>
               </div>
               <div class="field-group">
                   <label for="menu-${menuLetter}-url">URL</label>
                   <input class="text long-field" type="text" id="menu-${menuLetter}-url"
                          name="menu-${menuLetter}-url" title="Menu ${menuLetter} URL"/>
                   <div class="error menu-${menuLetter}-url-error"></div>
               </div>
               <div class="field-group">
                   <label for="menu-${menuLetter}-popupmenu">Popup Menu</label>
                   <textarea class="textarea long-field" id="menu-${menuLetter}-popupmenu"
                          name="menu-${menuLetter}-popupmenu" title="Label ${menuLetter} Popup"></textarea>
                   <div class="error menu-${menuLetter}-popupmenu-error"></div>
               </div>
            #end
             <div class="field-group right-button">
                 <div class="buttons-container right-button">
                     <input id="save-config" class="button submit" type="submit" value="Save"/>
                 </div>
            </div>
        </form>

        <h1>Configuration Backup and Restore</h1>
        <p>The configuration files can be exported and imported here.  The format is an xml file
        where the values are base64 encoded.</p>

        <div class="config-input-container">
            <div class="config-input-row">
                <div class="config-input-cell">
                    <h2>Export</h2>
                    <p><a href="$baseUrl/plugins/servlet/header-menus/admin/config/get">Export Configuration</a></p>
                </div>
                <div class="config-input-cell">
                    <h2>Import</h2>
                    <form class="aui" method="post"
                                      action="$baseUrl/plugins/servlet/header-menus/admin/configuration"
                                      enctype = "multipart/form-data" >
                        <fieldset>
                            <legend><span>Upload Config</span></legend>
                            <div class="field-group">
                                <label for="upload-config">Upload Config</label>
                                <input class="upfile" type="file" id="upload-config" name="upload-config">
                            </div>
                        </fieldset>
                        <div class="buttons-container">
                            <div class="buttons">
                                <button class="aui-button" id="upload-config-files">Upload File</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>

