guidanceConfigHelper = (function (jQuery) {

    var methods = new Object();
    var url = AJS.contextPath() + "/rest/guidance/1.0/admin-config/configuration";

    methods['loadConfig'] = function(){
       jQuery.ajax({
           url: url,
           dataType: "json"
       }).done(function(pluginConfiguration) { 
           var xml;

           if( pluginConfiguration.xml ){
              $xml = jQuery( jQuery.parseXML( decodeURIComponent(pluginConfiguration.xml) ) );
           } else {
              var xmlString = '<?xml version="1.0" encoding="UTF-8"?>' + "\n"
                            + '<plugin-configuration>' + "\n"
                            + '   <glyph-rendering-defaults>' + "\n"
                            + "      <for-html>false</for-html>" + "\n"
                            + "      <for-pdf>true</for-pdf>" + "\n"
                            + "      <for-word>false</for-word>" + "\n"
                            + "      <for-scroll>false</for-scroll>" + "\n"
                            + '   </glyph-rendering-defaults>' + "\n"
                            + '</plugin-configuration>' + "\n";
              $xml = jQuery( jQuery.parseXML( xmlString ) );
           }

           var forHtml = $xml.find( "for-html" );
           if( forHtml.length > 0 ){
              if( jQuery(forHtml[0]).text() === "true" ){
                 jQuery("#for-html").prop('checked', true );
              } else {
                 jQuery("#for-html").prop('checked', false );
              }
           } else {
              jQuery("#for-html").prop('checked', false );
           }

           var forPdf = $xml.find( "for-pdf" );
           if( forPdf.length > 0 ){
              if( jQuery(forPdf[0]).text() === "false" ){
                 jQuery("#for-pdf").prop('checked', false );
              } else {
                 jQuery("#for-pdf").prop('checked', true );
              }
           } else {
              jQuery("#for-pdf").prop('checked', true );
           }

           var forWord = $xml.find( "for-word" );
           if( forWord.length > 0 ){
              if( jQuery(forWord[0]).text() === "true" ){
                 jQuery("#for-word").prop('checked', true );
              } else {
                 jQuery("#for-word").prop('checked', false );
              }
           } else {
              jQuery("#for-word").prop('checked', false );
           }

           var forScroll = $xml.find( "for-scroll" );
           if( forScroll.length > 0 ){
              if( jQuery(forScroll[0]).text() === "true" ){
                 jQuery("#for-scroll").prop('checked', true );
              } else {
                 jQuery("#for-scroll").prop('checked', false );
              }
           } else {
              jQuery("#for-scroll").prop('checked', false );
           }

       }).fail(function(self,status,error){
          alert( error );
       });

    }

    methods['saveConfig'] = function(){

       var forHtmlStatus = "false";
       var forPdfStatus  = "true";
       var forWordStatus = "false";
       var forScrollStatus = "false";

       if( jQuery("#for-html").is(":checked") ){
          forHtmlStatus = "true";
       } else {
          forHtmlStatus = "false";
       }

       if( jQuery("#for-pdf").is(":checked") ){
          forPdfStatus = "true";
       } else {
          forPdfStatus = "false";
       }

       if( jQuery("#for-word").is(":checked") ){
          forWordStatus = "true";
       } else {
          forWordStatus = "false";
       }

       if( jQuery("#for-scroll").is(":checked") ){
          forScrollStatus = "true";
       } else {
          forScrollStatus = "false";
       }

       var xmlString = '<?xml version="1.0" encoding="UTF-8"?>' + "\n"
                     + '<plugin-configuration>' + "\n"
                     + '   <glyph-rendering-defaults>' + "\n"
                     + "      <for-html>" + forHtmlStatus + "</for-html>" + "\n"
                     + "      <for-pdf>" + forPdfStatus + "</for-pdf>" + "\n"
                     + "      <for-word>" + forWordStatus + "</for-word>" + "\n"
                     + "      <for-scroll>" + forScrollStatus + "</for-scroll>" + "\n"
                     + '   </glyph-rendering-defaults>' + "\n"
                     + '</plugin-configuration>' + "\n";

       jQuery.ajax({
          url: url,
          type: "PUT",
          contentType: "application/json",
          data: '{"xml":"' + encodeURIComponent(xmlString) + '"}',
          processData: false
       }).done(function () { 
       }).fail(function (self, status, error) { alert(error); 
       });
    }

    return methods;
})(jQuery);

AJS.toInit(function() {

   jQuery("#for-html").click(function() {
      guidanceConfigHelper.saveConfig();
   });

   jQuery("#for-pdf").click(function() {
      guidanceConfigHelper.saveConfig();
   });

   jQuery("#for-word").click(function() {
      guidanceConfigHelper.saveConfig();
   });

   jQuery("#for-scroll").click(function() {
      guidanceConfigHelper.saveConfig();
   });

   guidanceConfigHelper.loadConfig();
});
