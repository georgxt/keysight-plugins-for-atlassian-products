keysightThemeConfigHelper = (function(jQuery) {

    var methods = new Object();
    var url = AJS.contextPath() + "/rest/keysight-theme/1.0/admin-config/configuration";
    var defaultServiceUrl = "https://api.is.keysight.com/cgi-bin/org/of_apps/apiManager/apiManager.cgi";

    // This dictates whether the stored xml is still valid.
    // Follows the Semver standard (First digit indicates breaking change).
    var schemaVersionString = "1.0.0";
    var schemaVersionArr = schemaVersionString.split(".");

    methods['saveConfig'] = function() {
        saveConfigInternal();
    }

    methods['loadConfig'] = function() {
        jQuery.ajax({
            url: url,
            dataType: "json"
        }).done(function(pluginConfiguration) {
            var defaultSchema = "<plugin-configuration>\n"
                              + "    <schema-version>"+btoa(schemaVersionString)+"</schema-version>\n";
            defaultSchema += "    <api-documentation-service-url>"+btoa(defaultServiceUrl)+"</api-documentation-service-url>\n";
            defaultSchema += "</plugin-configuration>";

            // If XML doesn't validate, just give an empty document
            try {
                $xml = jQuery(jQuery.parseXML(decodeURIComponent(pluginConfiguration.xml)));
            } catch(err) {
                if( pluginConfiguration.xml != null){
                   console.error("Failed to parse XML: ", err);
                }
                $xml = jQuery(jQuery.parseXML(defaultSchema));
            }

            // Check for breaking changes in the schema
            if ($xml.find("schema-version").length != 0) {
                var xmlVersion = $xml.find("schema-version").html();
                var xmlMajorVer = xmlVersion.split(".")[0];
                if (xmlMajorVer != schemaVersionArr[0]) {
                    $xml = jQuery(jQuery.parseXML(defaultSchema));
                }
            }

            if( $xml.find("api-documentation-service-url").length > 0 ){
               var buffer = $xml.find("api-documentation-service-url").html().trim();
               if( buffer && !(buffer.length === 0) )
               {
                   try {
                      buffer = atob(buffer);
                      jQuery("#api-documentation-service-url").val( buffer );
                   } catch( error ) {
                      alert( "Unable to convert the api-documentation-service-url from base64:" + buffer + "\n" + error);
                   }
               } else {
                   jQuery("#api-documentation-service-url").val( defaultServiceUrl );
               }
            } else {
               jQuery("#api-documentation-service-url").val( defaultServiceUrl );
            }
        }).fail(function(self, status, error) {
            var loadFlag = AJS.flag({
                type: 'error',
                title: 'Failed to fetch configuration.',
                body: 'Please try again. If problems persist, contact your Confluence administrator.',
                close: 'auto'
            });
        });
    }

    function saveConfigInternal() {
        var xmlString = '<?xml version="1.0" encoding="UTF-8"?>' + "\n" +
            '<plugin-configuration>' + "\n" +
               '<schema-version>'+schemaVersionString+'</schema-version>' + "\n";

        var buffer = jQuery("#api-documentation-service-url").val();
        if( buffer && !(buffer.length === 0) )
        {
           try {
                buffer = btoa(buffer);
           } catch(error) {
                alert( "Unable to convert the api-documentation-service-url to base64:" + buffer + "\n" + error);
           }
           xmlString += "   <api-documentation-service-url>"+buffer+"</api-documentation-service-url>\n";
        } else {
           xmlString += "   <api-documentation-service-url>"+btoa(defaultServiceUrl)+"</api-documentation-service-url>\n";
        }

        xmlString += '</plugin-configuration>' + "\n";

        // Validate XML
        try {
            jQuery(jQuery.parseXML(xmlString))
        } catch(err) {
            console.error("Malformed XML!: ", err);
            return;
        }

        jQuery.ajax({
            url: url,
            type: "PUT",
            contentType: "application/json",
            data: '{"xml":"' + encodeURIComponent(xmlString) + '"}',
            processData: false
        }).done(function() {
            var saveSuccessFlag = AJS.flag({
                type: 'success',
                title: 'Success!',
                body: 'Plugin configuration was saved successfully.',
                close: 'auto'
            });
        }).fail(function(self, status, error) {
            var saveFailFlag = AJS.flag({
                type: 'error',
                title: 'Failed to save configuration.',
                body: 'Please try again. If problems persist, contact your Confluence administrator.',
                close: 'auto'
            });
        });
    }

    return methods;
})(jQuery);

AJS.toInit(function() {

    jQuery("#save-config").click(function(e) {
        e.preventDefault();
        keysightThemeConfigHelper.saveConfig();
    });

    keysightThemeConfigHelper.loadConfig();
});
