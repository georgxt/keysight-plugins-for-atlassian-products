package com.keysight.keysight.theme.helpers;

import com.atlassian.sal.api.pluginsettings.PluginSettings;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.sal.api.transaction.TransactionCallback;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.codec.binary.Base64;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.StringReader;
import java.net.URL;
import java.net.URLClassLoader;
import java.net.URLDecoder;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Driver;
import java.sql.DriverManager;
import java.util.*;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;

@Component
public class PluginConfigManager {
    private static final Logger log = LoggerFactory.getLogger(PluginConfigManager.class);

    private Element configRoot;
    private String configXml;
    private String apiDocumentationServiceUrl;

    private final PluginSettingsFactory pluginSettingsFactory;
    private final TransactionTemplate transactionTemplate;

    @Autowired
    public PluginConfigManager(final PluginSettingsFactory pluginSettingsFactory,
                               final TransactionTemplate transactionTemplate) {
        this.pluginSettingsFactory = pluginSettingsFactory;
        this.transactionTemplate = transactionTemplate;
        this.loadFromStorage();
    }

    public void loadFromStorage()
    {
        DocumentBuilder builder;
        Document document;
        try {
            this.loadConfigXmlFromStorage();
            builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            document = builder.parse(new InputSource(new StringReader(configXml)));
            configRoot = document.getDocumentElement();
        } catch (Exception exception) {
            log.warn("Unable to load the Keysight Theme plugin configuration:" + exception);
        }
    }

    private void loadConfigXmlFromStorage()
    {
        String defaultServer = "";

        try {
            byte[] encodedBytes = Base64.encodeBase64(KeysightThemeConstants.getDefaultApiDocumentationServiceUrl().getBytes());
            defaultServer = new String(encodedBytes, "UTF-8");
        } catch( Exception exception ){
            log.warn("Unable to encode the default api documentation sevice url:" + exception);
        }

        String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                + "<plugin-configuration>\n";
        xml += "   <api-documentation-service-url>"+defaultServer+"</api-documentation-service-url>\n";
        xml += "</plugin-configuration>\n";

        try {
            PluginConfigContainer pluginConfigContainer = (PluginConfigContainer) transactionTemplate.execute((TransactionCallback<Object>) () -> {
                PluginSettings settings = pluginSettingsFactory.createGlobalSettings();
                PluginConfigContainer innerPluginConfigContainer = new PluginConfigContainer();
                innerPluginConfigContainer.setXml((String) settings.get(PluginConfigManager.class.getName() + ".xml"));
                return innerPluginConfigContainer;
            });

            if (pluginConfigContainer.getXml() != null && !StringUtils.isEmpty( pluginConfigContainer.getXml() )) {
                xml = pluginConfigContainer.getXml();
            }
        }
        catch( Exception exception )
        {
            log.warn( "Failed to retrieve or parse the xml config: " + exception.getMessage() );
        }

        this.apiDocumentationServiceUrl = null;
        this.configXml = xml;
    }

    public boolean setConfigXml( String xml )
    {
        boolean updated = false;
        if( this.configXmlIsValid( xml ) ) {
            updated = true;
            transactionTemplate.execute(() -> {
                PluginSettings pluginSettings = pluginSettingsFactory.createGlobalSettings();
                pluginSettings.put(PluginConfigManager.class.getName() + ".xml", xml);
                return null;
            });
            this.loadFromStorage();
        }
        return updated;
    }

    public String getConfigXml()
    {
        return configXml;
    }

    public String getApiDocumentationServiceUrl()
    {
        if( this.apiDocumentationServiceUrl == null ){
            try {
                String encodedValue = this.getValueOfElement("api-documentation-service-url");
                this.apiDocumentationServiceUrl = new String(Base64.decodeBase64(encodedValue), "UTF-8");
            } catch( Exception exception ){
                this.apiDocumentationServiceUrl = KeysightThemeConstants.getDefaultApiDocumentationServiceUrl();
                log.warn( "Failed to retrieve the api documentation service url from the saved config. Using the default.\n" + exception.getMessage() );
            }
        }
        return this.apiDocumentationServiceUrl;
    }

    private boolean configXmlIsValid( String xml )
    {
        boolean flag = true;
        try {
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document document = builder.parse(new InputSource(new StringReader(xml)));
            Element root = document.getDocumentElement();
        }
        catch( Exception exception )
        {
            flag = false;
        }
        return flag;
    }

    private String getValueOfElement(String tagName) {
        NodeList allNodes = configRoot.getElementsByTagName(tagName);
        String text = null;
        for (int i = 0; i < allNodes.getLength(); i++) {
            if (allNodes.item(i).getNodeType() == Node.ELEMENT_NODE) {
                text = allNodes.item(i).getTextContent();
                break;
            }
        }
        return text;
    }
}

