package com.keysight.html.elements.macros; 

import java.util.Map;
import java.util.Date;
import java.util.List;
import java.util.ArrayList;
import java.text.SimpleDateFormat;
import org.apache.commons.lang3.StringUtils;

import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;

import com.atlassian.renderer.RenderContext;
import com.atlassian.renderer.v2.RenderMode;
import com.atlassian.renderer.v2.macro.MacroException;
import com.atlassian.renderer.v2.macro.BaseMacro;

import com.atlassian.confluence.core.ContentEntityObject;

public class PageInfo extends BaseMacro implements Macro
{
    private static final String INCLUDE_DESCRIPTIVE_TEXT       = "include-descriptive-text";
    private static final String LIST_REPRESENTATION            = "list-representation";
    private static final String UNORDERED_LIST                 = "Unordered List";
    private static final String NEW_LINE                       = "New Line";
    private static final String COMMA_SEPARATED                = "Comma Separated";
    private static final String INCLUDE_PAGE_ID                = "include-page-id";
    private static final String INCLUDE_ORIGINAL_AUTHOR        = "include-original-author";
    private static final String INCLUDE_MOST_RECENT_AUTHOR     = "include-latest-author";
    private static final String INCLUDE_CREATION_DATE          = "include-creation-date";
    private static final String INCLUDE_LAST_MODIFICATION_DATE = "include-last-modification-date";

    private final SimpleDateFormat LONG_DATE_FORMAT = new SimpleDateFormat( "EEEE MMMM d, YYYY" );

    public PageInfo()
    {
    }

    @Override
    public String execute(Map<String, String> parameters, String body, ConversionContext context)
            throws MacroExecutionException
    {
        StringBuilder html    = new StringBuilder();
        ContentEntityObject entity = context.getEntity();
        boolean commaSeparated = true;
        boolean newLineSeparated = false;
        boolean listSeparated = false;
        String listRepresentation = "Comma Separated";
        List<String> items = new ArrayList<String>();

        if( parameters.containsKey( LIST_REPRESENTATION ) ){
            listRepresentation = parameters.get( LIST_REPRESENTATION );
        }

        if( listRepresentation.matches(UNORDERED_LIST)) {
            commaSeparated = false;
            newLineSeparated = false;
            listSeparated = true;
        } else if( listRepresentation.matches(NEW_LINE)){
            commaSeparated = false;
            newLineSeparated = true;
            listSeparated = false;
        }

        if( listSeparated ){
            html.append( "<ul>\n" );

            if( parameters.containsKey( INCLUDE_PAGE_ID )){
                html.append( "   <li>" + getPageId( parameters, entity ) + "</li>\n" );
            }

            if( parameters.containsKey( INCLUDE_ORIGINAL_AUTHOR )){
                html.append( "   <li>" + getOriginalAuthor( parameters, entity ) + "</li>\n" );
            }

            if( parameters.containsKey( INCLUDE_CREATION_DATE )){
                html.append( "   <li>" + getCreationDate( parameters, entity ) + "</li>\n" );
            }

            if( parameters.containsKey( INCLUDE_MOST_RECENT_AUTHOR )){
                html.append( "   <li>" + getMostRecentAuthor( parameters, entity ) + "</li>\n" );
            }

            if( parameters.containsKey( INCLUDE_LAST_MODIFICATION_DATE )){
                html.append( "   <li>" + getLastModificationDate( parameters, entity ) + "</li>\n" );
            }

            html.append( "</ul>\n" );
        } else {

            if( parameters.containsKey( INCLUDE_PAGE_ID )){
                items.add( getPageId( parameters, entity ) );
            }

            if( parameters.containsKey( INCLUDE_ORIGINAL_AUTHOR )){
                items.add( getOriginalAuthor( parameters, entity ) );
            }

            if( parameters.containsKey( INCLUDE_CREATION_DATE )){
                items.add( getCreationDate( parameters, entity ) );
            }

            if( parameters.containsKey( INCLUDE_MOST_RECENT_AUTHOR )){
                items.add( getMostRecentAuthor( parameters, entity ) );
            }

            if( parameters.containsKey( INCLUDE_LAST_MODIFICATION_DATE )){
                items.add( getLastModificationDate( parameters, entity ) );
            }

            if( newLineSeparated ) {
                html.append(String.join("<br />\n", items));
            } else {
                html.append(String.join(",", items));
            }
        }

	    return html.toString();
    }

    private String getPageId(Map<String, String> parameters, ContentEntityObject entity ){
        String html = null;
        if( parameters.containsKey(INCLUDE_DESCRIPTIVE_TEXT)){
            html = "The page ID is " + entity.getIdAsString();
        } else {
            html = entity.getIdAsString().toString();
        }
        return html;
    }

    private String getOriginalAuthor(Map<String, String> parameters, ContentEntityObject entity ){
        String html = null;
        if( parameters.containsKey(INCLUDE_DESCRIPTIVE_TEXT)){
            html = "The original author is " + entity.getCreatorName();
        } else {
            html = entity.getCreatorName();
        }
        return html;
    }

    private String getMostRecentAuthor(Map<String, String> parameters, ContentEntityObject entity ){
        String html = null;
        if( parameters.containsKey(INCLUDE_DESCRIPTIVE_TEXT)){
            html = "The last change was made by " + entity.getLastModifierName();
        } else {
            html = entity.getLastModifierName();
        }
        return html;
    }

    private String getCreationDate(Map<String, String> parameters, ContentEntityObject entity ){
        String html = null;
        if( parameters.containsKey(INCLUDE_DESCRIPTIVE_TEXT)){
            html = "The first version was created on " + LONG_DATE_FORMAT.format(entity.getCreationDate());
        } else {
            html = LONG_DATE_FORMAT.format(entity.getCreationDate());
        }
        return html;
    }

    private String getLastModificationDate(Map<String, String> parameters, ContentEntityObject entity ){
        String html = null;
        if( parameters.containsKey(INCLUDE_DESCRIPTIVE_TEXT)){
            html = "The content was last modified on " + LONG_DATE_FORMAT.format(entity.getLastModificationDate());
        } else {
            html = LONG_DATE_FORMAT.format(entity.getLastModificationDate());
        }
        return html;
    }

    @Override
    public BodyType getBodyType()
    {
        return BodyType.NONE;
    }

    @Override
    public OutputType getOutputType()
    {
        return OutputType.BLOCK;
    }

    public boolean hasBody()
    {
        return false;
    }

    // Used to work with wiki syntax mode.
    public RenderMode getBodyRenderMode()
    {
        return RenderMode.ALL;
    }

    public String execute(Map params, String body, RenderContext renderContext) throws MacroException
    {
        try
        {
            return execute(params, body, new DefaultConversionContext( renderContext ));
        }
        catch(MacroExecutionException e)
        {
            throw new MacroException(e);
        }
    }
}
