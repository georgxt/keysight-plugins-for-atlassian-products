<h3>Introduction</h3>

<p>The Macro Results Cache will cache the rendered output of the macro for some window of time.  The <b>Database
Connection</b> plugin configuration controls the maximum lifetime of any cache entry.  Using the <b>Cache Expiration</b>
parameter, the global value can be reduced for an instance of this macro.  While thie macro can be used to cache the
output of any macro, the use case in mind is to wrap a <b>Chart</b> macro containing a <b>Database Query</b> macro.
Thus instead of caching the database table, the rendered graph is cached.</p>

<h3>Confluence Data Cluster</h3>
<p>When used in Confluence Data Center, each of the nodes will have it's own cached copy of the results.  This is because
the temp folder where some macro such as the **Chart** macro create and server their images from are on the cluster
node file system rather than the shared file system.  Thus, other sibling nodes can't access the images created by
a node.  By having a cache for each node, the cache works mostly as expected.  The one unexpected result is the
cached information may differ.  For example, if a chart wrapped with a **Macro Results Cache** macro is viewed on
one node, then the data changes and then it's viewed on a different node.  The view from the two nodes will be different
until either the cache for the first node expires or somebody manually presses the **Refresh** link causing the cache
for both nodes to be reset.</p>

<h3>Parameters</h3>
<p><strong>Cache Expiration</strong>:  The number of minutes to cache results.  The results of a query will be cached
 for this number of minutes.  Any additional renderings of the macro within this window will just recall the cached
 results.  When the number of minutes, the database will be queried again and the cycle restarts.  A empty value or 0
 will result in no caching of results.</p>
<p><strong>Show Bounding Box</strong>: Add a box with a refresh button around the cached content.</p>
<p><strong>Show Refresh Button</strong>: Add an icon to refresh the content and cache with the latest content.</p>
