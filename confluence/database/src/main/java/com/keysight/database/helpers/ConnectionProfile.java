package com.keysight.database.helpers;


import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class ConnectionProfile {
    @XmlElement
    public String authorizedUsers;

    @XmlElement
    public String authorizedGroups;

    @XmlElement
    public String authorizedSpaces;

    @XmlElement
    private String connectionStringSuffix;

    @XmlElement
    private String databaseName;

    @XmlElement
    private String databasePassword;

    @XmlElement
    private String databasePort;

    @XmlElement
    private String databaseServer;

    @XmlElement
    private String databaseType;

    @XmlElement
    private String databaseUsername;

    @XmlElement
    private String profileDescription;

    @XmlElement
    private String profileId;

    @XmlElement
    private String profileName;

    // Dummy constructor to make JAXB happy
    public ConnectionProfile() {
    }

    public ConnectionProfile(Element profile) {
        try {
            profileName = GetValueOfElementWithName(profile, "profile-name");
            profileId = GetValueOfElementWithName(profile, "profile-id");
            profileDescription = GetValueOfElementWithName(profile, "profile-description");
            authorizedUsers = GetValueOfElementWithName(profile, "authorized-users");
            authorizedGroups = GetValueOfElementWithName(profile, "authorized-groups");
            authorizedSpaces = GetValueOfElementWithName(profile, "authorized-spaces");
            databaseType = GetValueOfElementWithName(profile, "database-type");
            databaseName = GetValueOfElementWithName(profile, "database-name");
            databaseServer = GetValueOfElementWithName(profile, "database-server");
            databasePort = GetValueOfElementWithName(profile, "database-port");
            databaseUsername = GetValueOfElementWithName(profile, "database-username");
            databasePassword = GetValueOfElementWithName(profile, "database-password");
            connectionStringSuffix = GetValueOfElementWithName(profile, "connection-string-suffix");
        } catch (Exception e) {
            System.out.println("Failed to get XML details");
        }
    }

    private ArrayList<String> trimList(ArrayList<String> untrimmedList)
    {
        ArrayList<String> trimmedList = new ArrayList<String>();
        for(String item : untrimmedList )
        {
            trimmedList.add(item.trim());
        }
        return(trimmedList);
    }

    public ArrayList<String> getAuthorizedUsers() {
        if(StringUtils.isEmpty(this.authorizedUsers))
        {
            return new ArrayList<String>();
        }
        else
        {
            return(trimList(new ArrayList<>(Arrays.asList(StringUtils.split(this.authorizedUsers, ",")))));
        }
    }

    public ArrayList<String> getAuthorizedGroups() {
        if(StringUtils.isEmpty(this.authorizedGroups))
        {
            return new ArrayList<String>();
        }
        else
        {
            return(trimList(new ArrayList<>(Arrays.asList(StringUtils.split(this.authorizedGroups, ",")))));
        }
    }

    public ArrayList<String> getAuthorizedSpaces() {
        if(StringUtils.isEmpty(this.authorizedSpaces))
        {
            return new ArrayList<String>();
        }
        else
        {
            return(trimList(new ArrayList<>(Arrays.asList(StringUtils.split(this.authorizedSpaces, ",")))));
        }
    }

    public boolean spaceIsAuthorizedToUseProfile(String spaceKey)
    {
        boolean spaceHasPermissionsToUseProfile = false;
        List<String> authorizedSpaces = this.getAuthorizedSpaces();
        if(authorizedSpaces.size() == 0)
        {
            spaceHasPermissionsToUseProfile = true;
        }
        else if(authorizedSpaces.contains(spaceKey))
        {
            spaceHasPermissionsToUseProfile = true;
        }

        return spaceHasPermissionsToUseProfile;
    }

    public boolean universalAccessAllowed() {
        boolean flag = false;
        if( StringUtils.isEmpty(this.authorizedUsers) 
            && StringUtils.isEmpty(this.authorizedGroups)
            && StringUtils.isEmpty(this.authorizedSpaces)){
            flag = true;
        }
        return flag;
    }

    public String getConnectionStringSuffix() {
        return connectionStringSuffix;
    }

    public String getDatabaseName() {
        return databaseName;
    }

    public String getDatabasePassword() {
        return databasePassword;
    }

    public String getDatabasePort() {
        return databasePort;
    }

    public String getDatabaseServer() {
        return databaseServer;
    }

    public  String getDatabaseType() {
        return databaseType;
    }

    public String getDatabaseUsername() {
        return databaseUsername;
    }

    public String getProfileDescription() {
        return profileDescription;
    }

    public String getProfileId() {
        return profileId;
    }

    public String getProfileName() {
        return profileName;
    }

    public String getId() {
        return getProfileId();
    }

    public String getName() {
        return getProfileName();
    }

    private String GetValueOfElementWithName(Element profile, String elementName)
    {
        String profileName = new String("");
        try {
            NodeList nodeList = profile.getElementsByTagName(elementName);
            if(nodeList.getLength() > 0)
            {
                profileName = new String(Base64.decodeBase64(nodeList.item(0).getTextContent()), "UTF-8");
            }
        } catch (Exception e) {
            System.out.println("Failed to get the value of xml element with name " + elementName + " from the connection profile.");
        }

        return profileName;
    }
}
