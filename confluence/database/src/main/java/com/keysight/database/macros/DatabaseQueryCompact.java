package com.keysight.database.macros;

import com.atlassian.cache.CacheManager;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.atlassian.core.task.MultiQueueTaskManager;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.atlassian.sal.api.user.UserKey;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.user.UserProfile;
import com.atlassian.confluence.plugin.services.VelocityHelperService;
import com.keysight.database.helpers.PluginConfigManager;

import static com.atlassian.confluence.mail.template.ConfluenceMailQueueItem.MIME_TYPE_HTML;

public class DatabaseQueryCompact extends DatabaseQuery {

    public DatabaseQueryCompact( CacheManager cacheManager,
                                 PageManager pageManager,
                                 PermissionManager permissionManager,
                                 PluginConfigManager pluginConfigManager,
                                 PluginSettingsFactory pluginSettingsFactory,
                                 SettingsManager settingsManager,
                                 SpaceManager spaceManager,
                                 MultiQueueTaskManager taskManager,
                                 TransactionTemplate transactionTemplate,
                                 UserAccessor userAccessor,
                                 UserManager userManager,
                                 VelocityHelperService velocityHelperService,
                                 XhtmlContent xhtmlUtils
    ) {
        super( cacheManager, pageManager, permissionManager, pluginConfigManager, pluginSettingsFactory,
               settingsManager, spaceManager, taskManager, transactionTemplate, 
               userAccessor, userManager, velocityHelperService, xhtmlUtils);
    }

    public BodyType getBodyType() { return BodyType.NONE; }
}
