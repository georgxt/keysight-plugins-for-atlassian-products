package com.keysight.database.helpers;

import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.atlassian.sal.api.user.UserKey;
import com.atlassian.sal.api.user.UserManager;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;

public class GetConfig extends HttpServlet {
    protected final UserManager userManager;
    protected final LoginUriProvider loginUriProvider;
    protected final PluginConfigManager pluginConfigManager;
    protected final PluginSettingsFactory pluginSettingsFactory;
    protected final TransactionTemplate transactionTemplate;

    public GetConfig(LoginUriProvider loginUriProvider,
                     PluginConfigManager pluginConfigManager,
                     PluginSettingsFactory pluginSettingsFactory,
                     TransactionTemplate transactionTemplate,
                     UserManager userManager) {
        this.loginUriProvider = loginUriProvider;
        this.pluginConfigManager = pluginConfigManager;
        this.pluginSettingsFactory = pluginSettingsFactory;
        this.transactionTemplate = transactionTemplate;
        this.userManager = userManager;
    }

    protected String getFileName()
    {
        return "dbConnectorConfig.xml";
    }

    protected String getXml()
    {
        return "<pluginConfig></pluginConfig>";
    }

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        if (!isAuthorized(request)) {
            redirectToLogin(request, response);
            return;
        }

        String xml = this.getXml();
        String fileName = this.getFileName();

        if( xml != null )
        {
            response.setContentType("text/xml;charset=utf-8");
            response.addHeader("Content-Disposition", "attachment; filename=" + fileName);
            response.addHeader("Content-Length", Integer.toString(xml.length()));
            PrintWriter writer = response.getWriter();
            writer.append( xml );
        }
    }

    private void redirectToLogin(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.sendRedirect(loginUriProvider.getLoginUri(getUri(request)).toASCIIString());
    }

    private URI getUri(HttpServletRequest request) {
        StringBuffer builder = request.getRequestURL();
        if (request.getQueryString() != null) {
            builder.append("?");
            builder.append(request.getQueryString());
        }
        return URI.create(builder.toString());
    }

    private boolean isAuthorized(HttpServletRequest request) {
        UserKey userKey = userManager.getRemoteUserKey(request);

        // Check basic conditions.
        if (userKey == null) {
            return false;
        } else if (userManager.isSystemAdmin(userKey)) {
            return true;
        }

        return false;
    }
}
