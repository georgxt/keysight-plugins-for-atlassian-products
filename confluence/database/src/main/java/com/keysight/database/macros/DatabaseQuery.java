package com.keysight.database.macros;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.storage.macro.MacroId;
import com.atlassian.confluence.content.render.xhtml.XhtmlException;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.search.service.ContentTypeEnum;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.mail.template.ConfluenceMailQueueItem;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.plugin.services.VelocityHelperService;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.confluence.xhtml.api.MacroDefinition;
import com.atlassian.confluence.xhtml.api.MacroDefinitionHandler;
import com.atlassian.confluence.xhtml.api.MacroDefinitionMarshallingStrategy;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.atlassian.core.task.MultiQueueTaskManager;
import com.atlassian.mail.queue.MailQueueItem;
import com.atlassian.sal.api.pluginsettings.PluginSettingsFactory;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.atlassian.sal.api.user.UserKey;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.user.UserProfile;
import com.keysight.database.helpers.DatabaseQueryHelper;
import com.keysight.database.helpers.ConnectionProfile;
import com.keysight.database.helpers.PluginConfigManager;
import com.keysight.database.helpers.InsertLogEntryIntoAuditDatabase;
import com.keysight.database.helpers.mail.MailService;
import com.keysight.database.helpers.mail.MailServiceImpl;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import com.atlassian.confluence.xhtml.api.MacroDefinition;
import com.atlassian.fugue.Option;
import com.atlassian.renderer.v2.RenderUtils;

import com.atlassian.cache.Cache;
import com.atlassian.cache.CacheLoader;
import com.atlassian.cache.CacheManager;
import com.atlassian.cache.CacheSettingsBuilder;

import java.util.concurrent.TimeUnit;
import java.sql.*;
import java.time.LocalDateTime;
import java.util.*;
import java.util.Date;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.atlassian.confluence.mail.template.ConfluenceMailQueueItem.MIME_TYPE_HTML;
import static com.atlassian.confluence.mail.template.ConfluenceMailQueueItem.MIME_TYPE_TEXT;

public class DatabaseQuery implements Macro {
    private static final Logger log = LoggerFactory.getLogger(DatabaseQuery.class);

    private final static String PROFILE = "profile";
    private final static String QUERY_ISVISIBLE = "show-query";
    private final static String COLLAPSE_QUERY = "collapse-query";
    private final static String SQL = "sql";
    private final static String CACHE_EXPIRATION = "cache-expiration";
    private final static String BOUNDING_BOX = "bounding-box";
    private final static String REFRESH_BUTTON = "refresh-button";
    private final static String HIDE_HEADER = "hide-header";

    protected final CacheManager cacheManager;
    protected final PageManager pageManager;
    protected final PermissionManager permissionManager;
    protected final PluginConfigManager pluginConfigManager;
    protected final PluginSettingsFactory pluginSettingsFactory;
    protected final SettingsManager settingsManager;
    protected final SpaceManager spaceManager;
    protected final MultiQueueTaskManager taskManager;
    protected final TransactionTemplate transactionTemplate;
    protected final UserAccessor userAccessor;
    protected final UserManager userManager;
    protected final VelocityHelperService velocityHelperService;
    protected final XhtmlContent xhtmlUtils;
    protected final MailService mailService;

    private Connection connect = null;
    private Statement statement = null;
    private ResultSet resultSet = null;

    protected Map<String, String> parameters = null;
    protected String body = null;
    protected ConversionContext context = null;
    protected Cache<String, String> dataCache = null;
    protected Cache<String, String> creationTimeCache = null;

    private String macroId = null;
    private String profileId = null;
    private String sql = null;

    public DatabaseQuery( CacheManager cacheManager,
                          PageManager pageManager,
                          PermissionManager permissionManager,
                          PluginConfigManager pluginConfigManager,
                          PluginSettingsFactory pluginSettingsFactory,
                          SettingsManager settingsManager,
                          SpaceManager spaceManager,
                          MultiQueueTaskManager taskManager,
                          TransactionTemplate transactionTemplate,
                          UserAccessor userAccessor,
                          UserManager userManager,
                          VelocityHelperService velocityHelperService,
                          XhtmlContent xhtmlUtils

    ) {
        this.cacheManager = cacheManager;
        this.pageManager = pageManager;
        this.permissionManager = permissionManager;
        this.pluginConfigManager = pluginConfigManager;
        this.pluginSettingsFactory = pluginSettingsFactory;
        this.settingsManager = settingsManager;
        this.spaceManager = spaceManager;
        this.taskManager = taskManager;
        this.transactionTemplate = transactionTemplate;
        this.userAccessor = userAccessor;
        this.userManager = userManager;
        this.velocityHelperService = velocityHelperService;
        this.xhtmlUtils = xhtmlUtils;

        this.mailService = new MailServiceImpl( taskManager );

        dataCache = cacheManager.getCache(DatabaseQuery.class.getName() + "data.cache",
                    new DatabaseQueryDataCacheLoader(),
                    new CacheSettingsBuilder().expireAfterWrite( pluginConfigManager.getMaxCacheLifeTimeInDaysAsLong(), TimeUnit.DAYS).build()
        );

        creationTimeCache = cacheManager.getCache(DatabaseQuery.class.getName() + ".create-time.cache",
                new DatabaseQueryCreateTimeCacheLoader(),
                new CacheSettingsBuilder().expireAfterWrite( pluginConfigManager.getMaxCacheLifeTimeInDaysAsLong(), TimeUnit.DAYS).build()
        );
    }

    private List<MacroDefinition> getMacrosOnPage(ContentEntityObject page) throws MacroExecutionException
    {
        final List<MacroDefinition> macros = new ArrayList<MacroDefinition>();
        try {
            xhtmlUtils.handleMacroDefinitions(page.getBodyContent().getBody(), context, new MacroDefinitionHandler(){
                @Override
                public void handle(MacroDefinition macroDefinition)
                {
                    macros.add(macroDefinition);
                }
            },
            MacroDefinitionMarshallingStrategy.MARSHALL_MACRO);
        } catch (XhtmlException e) {
           throw new MacroExecutionException(e);
        }

        return macros;
    }

    private MacroDefinition getSavedMacroDefinitionById(String macroId) throws MacroExecutionException
    {
        MacroDefinition savedMacroDefinition = null;

        List<MacroDefinition> savedMacros = new ArrayList<MacroDefinition>();
        try {
            savedMacros = getMacrosOnPage(this.context.getEntity());
        } catch(Exception exception) {
            throw exception;
        }

        for( MacroDefinition savedMacro : savedMacros ){
            try
            {
                Option<MacroId> savedMacroOption = savedMacro.getMacroId();
                String savedMacroId = savedMacroOption.get().getId();

                if(savedMacroId.equals(macroId))
                {
                    savedMacroDefinition = savedMacro;
                    break;
                }
            } 
            catch(Exception exception)
            {
                System.out.println("Could not locate a macroId from a saved macro.");
            }
        }

        return savedMacroDefinition;
    }

    private String getMacroId(ConversionContext context)
    {
        String macroId = null;
        try
        {
            MacroDefinition macroDefinition = (MacroDefinition)context.getProperty("macroDefinition");
            Option<MacroId> option = macroDefinition.getMacroId();
            macroId = option.get().getId();
        } 
        catch(Exception exception)
        {
            // When called from the macro browser preview, there is macro id.
        }

        return macroId;
    }

    private int getCacheExpiration(Map<String, String> parameters)
    {
        int cacheExpiration = 0;
        try 
        {
            if(parameters.containsKey(CACHE_EXPIRATION)) 
            {
                cacheExpiration = Integer.parseInt(parameters.get(CACHE_EXPIRATION));
            }
        } 
        catch( Exception exception )
        {
        }

        return cacheExpiration;
    }


        //Map<String, String> macroParameters = macroDefinition.getParameters();
        //System.out.println("Macro Parameters");
        //for( Map.Entry<String,String> entry : macroParameters.entrySet()){
            //System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue());
        //}

        //List<MacroDefinition> macros = new ArrayList<MacroDefinition>();
        //MacroDefinition savedMacroDefinition = null;
        //try {
            //savedMacroDefinition = getSavedMacroById(this.context.getEntity(), context);
        //} catch(Exception exception) {
            //throw exception;
        //}
        /*
        Map<String, String> savedMacroParameters = savedMacroDefinition.getParameters();
        String savedSql = "no sql";
        if( savedMacroParameters.containsKey(SQL) ) {
            savedSql = savedMacroParameters.get( SQL );
        } else {
            savedSql = savedMacroDefinition.getBodyText();
        }
        System.out.println("SQL Saved with the Page: " + savedSql);
*/

    private boolean currentUserHasPermissionsForProfile(ConnectionProfile profile)
    {
        ConfluenceUser currentUser = AuthenticatedUserThreadLocal.get();
        boolean userHasPermissions = false;
        System.out.println("Check if " + currentUser.getName() + " has access to the profile.");

        if(profile.universalAccessAllowed())
        {
            System.out.println("The profile allows universal access.");
            userHasPermissions = true;
        }
        else
        {
            UserProfile currentProfile = userManager.getUserProfile(currentUser.getKey());
            String currentUsername = currentProfile.getUsername();

            List<String> authorizedUsers = profile.getAuthorizedUsers();
            System.out.println("Test if " + currentUsername + " is an authorized user for the profile");
            if (profile.getAuthorizedUsers().contains(currentUsername)) 
            {
                System.out.println("The user, \"" + currentUsername + "\", has named access to the profile");
                userHasPermissions = true;
            }
            else
            {
                List<String> currentGroups = userAccessor.getGroupNamesForUserName(currentUsername);
                List<String> authorizedGroups = profile.getAuthorizedGroups();
                authorizedGroups.retainAll(currentGroups); // Shared elements between allowed groups and groups user is in.
                if(!authorizedGroups.isEmpty()) // If none of the user's groups are allowed, reject them.
                {
                    System.out.println("The user, \"" + currentUsername + "\", has group access to the profile");
                    userHasPermissions = true;
                }
            }
        }

        return(userHasPermissions);
    }

    public boolean allowedContentType(ConversionContext context)
    {
        ContentEntityObject contentEntityObject = context.getEntity();
        ContentTypeEnum contentEntityObjectType = contentEntityObject.getTypeEnum();
        ArrayList<ContentTypeEnum> allowedContentTypes = new ArrayList<ContentTypeEnum>();
        allowedContentTypes.add(ContentTypeEnum.BLOG);
        allowedContentTypes.add(ContentTypeEnum.DRAFT);
        allowedContentTypes.add(ContentTypeEnum.PAGE);
        return(allowedContentTypes.contains(contentEntityObjectType));
    }

    public String execute(Map<String, String> parameters, String body, ConversionContext context) throws MacroExecutionException 
    {

        this.parameters = parameters;
        this.body = body;
        this.context = context;

        this.macroId = getMacroId(context);
        int cacheExpiration = getCacheExpiration(parameters);

        if(!allowedContentType(context))
        {
            throw new MacroExecutionException("This macro may only be used on pages and blog posts.\n");
        }

        //System.out.println( "\n\n\nCACHE Expirarion: "  + cacheExpiration + ")\n\n\n" );

        if( this.macroId == null ) {
            System.out.println("No macro ID");

            return tryRenderDatabaseQuery(this.macroId);
        } 
        else if( cacheExpiration > 0 ) 
        {
            try
            {
                // Clear the cache entry if it's older than the window set in the macro properties
                long creationTime = Long.parseLong(creationTimeCache.get(this.macroId));
                if(creationTime + ((long)cacheExpiration * (long)60 * (long)1000 ) < System.currentTimeMillis())
                {
                    //System.out.println("Clear " + this.macroId + " From the cache\n" );
                    creationTimeCache.remove(this.macroId);
                    dataCache.remove(this.macroId);
                }

                System.out.println("Try to get Data from CACHE (MacroID: " + macroId + ")\n");
                return dataCache.get(this.macroId);
            } 
            catch (NoSuchElementException exception) 
            {
                System.out.println("Cache Retrival aborted: " + exception.getMessage() + "\n");
                return tryRenderDatabaseQuery(this.macroId);
            }
        }
        else
        {
            System.out.println("Render the sql with macroId: " + this.macroId );
            return tryRenderDatabaseQuery(this.macroId);
        }
    }

    private class DatabaseQueryCreateTimeCacheLoader implements CacheLoader<String, String>
    {
        @Override
        public String load(String creationTimeInMillisFromLong)
        {
            return Long.toString(System.currentTimeMillis());
        }
    }

    private class DatabaseQueryDataCacheLoader implements CacheLoader<String, String>
    {
        @Override
        public String load(String macroId)
        {
            return tryRenderDatabaseQuery(macroId);
        }
    }

    private String tryRenderDatabaseQuery(String macroId)
    {
        try
        {
            return renderDatabaseQuery(macroId);
        }
        catch( Exception exception )
        {
            return RenderUtils.blockError("Macro Execution Error:", exception.getMessage());
        }
    }

    private String getSql(MacroDefinition macroDefinition)
    {
        String sql = null;
        Map<String, String> macroParameters = macroDefinition.getParameters();
        if( macroParameters.containsKey(SQL) ) {
            sql = macroParameters.get(SQL);
        } else {
            sql = macroDefinition.getBodyText();
        }

        return sql;
    }

    private String getActiveProfileId(MacroDefinition macroDefinition)
    {
        String activeProfileId = null;
        Map<String, String> macroParameters = macroDefinition.getParameters();
        if( macroParameters.containsKey(PROFILE) ) {
            activeProfileId = macroParameters.get(PROFILE);
        }

        return activeProfileId;
    }

    public String renderDatabaseQuery(String macroId) throws MacroExecutionException 
    {
        String baseUrl = settingsManager.getGlobalSettings().getBaseUrl();
        String template = "/com/keysight/database/templates/database-query.vm";
        Map<String, Object> velocityContext = velocityHelperService.createDefaultVelocityContext();
        ConnectionProfile profile = null;
        MacroDefinition macroDefinition = null;

        String spaceKey = this.context.getSpaceKey();
        String spaceName = spaceManager.getSpace( spaceKey ).getName();
        String pageName = this.context.getEntity().getTitle();
        String pageUrl = baseUrl + this.context.getEntity().getUrlPath();

        ArrayList<String> columnNames = new ArrayList<>();
        ArrayList<ArrayList<String>> dataRows = new ArrayList<>();
        String fullName = null;
        String userName = null;
        ConfluenceUser currentUser = AuthenticatedUserThreadLocal.get();

        if (currentUser != null) {
            fullName = currentUser.getFullName();
            userName = currentUser.getName();
        } else {
            fullName = "Anonymous";
            userName = "anonymous";
        }

        if( macroId != null )
        {
            macroDefinition = getSavedMacroDefinitionById(macroId);

            try {
                creationTimeCache.remove(macroId);
            }
            catch( Exception exception ) {
            }
            creationTimeCache.get(macroId);
        } 
        else
        {
            // This is coming from the macro browser preview.
            macroDefinition = (MacroDefinition)this.context.getProperty("macroDefinition");
        }

        String sql = getSql(macroDefinition);
        String activeProfileId = getActiveProfileId(macroDefinition);

        // Clean up body by removing extra whitespace, newlines, etc.
        String rawSql = sql;
        sql = sql.replaceAll("\\s+", " ");

        //System.out.println("This is the SQL I want to use: " + sql);
        //System.out.println("This is the profile id want to use: " + activeProfileId);

        // needed for Confluence Data Center as the config could have been
        // updated by an instance of Confluence other than this one.
        pluginConfigManager.loadFromStorage();
        if( activeProfileId != null )
        {
            profile = pluginConfigManager.getConnectionProfileFromId( activeProfileId );
            if (profile == null) {
                String escapedActiveProfileId = activeProfileId.replace(">", "&gt;").replace("<", "&lt;");
                throw new MacroExecutionException("No profile found with that ID for which you are authorized: " + escapedActiveProfileId);
            }
        } else {
            throw new MacroExecutionException("No profile selected");
        }

        if(!profile.spaceIsAuthorizedToUseProfile(this.context.getSpaceKey()))
        {
            throw new MacroExecutionException("The selected database profile may not be used from this space.");
        }

        if( macroId == null )
        {
            // This is coming from the macro browser preview.
            if(!currentUserHasPermissionsForProfile(profile))
            {
                throw new MacroExecutionException("The current user does not have permissions to use this profile.");
            }
        }

        connect = DatabaseQueryHelper.createConnection(profile, pluginConfigManager);
        if (connect != null) {
            try {
                // Statements allow to issue SQL queries to the database
                statement = connect.createStatement();

                // Check for query constraints [pre]
                // We will set the soft timer arbitrarily high
                int softTimer = Integer.MAX_VALUE;
                int startQuery = (int) ((new Date()).getTime() / 1000); // Current time in seconds
                if (!Objects.equals(pluginConfigManager.getTimeoutLimitValue(), "")) {
                    // Try to get integer value of timeout value, default to 0
                    int timeVal;
                    try {
                        timeVal = Integer.parseInt(pluginConfigManager.getTimeoutLimitValue());
                    } catch (NumberFormatException e) {
                        // 0 seconds is interpreted as no time limit.
                        throw new MacroExecutionException("Failed to parse query timeout: " + pluginConfigManager.getTimeoutLimitValue());
                    }

                    // Soft or hard?
                    if (Objects.equals(StringUtils.lowerCase(pluginConfigManager.getTimeoutLimitType()), "soft")) {
                        softTimer = timeVal;
                    } else {
                        statement.setQueryTimeout(timeVal);
                    }
                }

                // Handle row limit
                int softRowLimit = Integer.MAX_VALUE;
                if (!Objects.equals(pluginConfigManager.getRowLimitValue(), "")) {
                    // Try to get integer value of timeout value, default to 0
                    int rowLimit;
                    try {
                        rowLimit = Integer.parseInt(pluginConfigManager.getRowLimitValue());
                    } catch (NumberFormatException e) {
                        // 0 seconds is interpreted as no time limit.
                        throw new MacroExecutionException("Failed to parse query row limit: " + pluginConfigManager.getTimeoutLimitValue());
                    }

                    // Soft or hard?
                    if (Objects.equals(StringUtils.lowerCase(pluginConfigManager.getRowLimitType()), "soft")) {
                        softRowLimit = rowLimit;
                    } else {
                        statement.setMaxRows(rowLimit);
                    }
                }

                // Result set get the result of the SQL query
                resultSet = statement.executeQuery(sql);

                // Check for query constraints [post]
                int endQuery = (int) ((new Date()).getTime() / 1000); // Current time in seconds

                // Did more than softTimer pass?
                if (endQuery - startQuery > softTimer) {
                    String message = "<p>The user <b>" + fullName + " ( " + userName + " )</b> executed an sql query\n "
                            + "using the Database Connection Macro on the page <a href=\"" + pageUrl + "\">" + pageName + "</a>\n "
                            + "in the space <b>" + spaceName + "</b>. It took " + (endQuery - startQuery) + " seconds\n"
                            + "exceeding the soft time limit of " + softTimer + ".</p>\n";

                    sendEmail(pluginConfigManager, "Confluence DB Connctory Soft Timout Limit Warning", message, null);
                }

                // Get the Column Names
                for (int i = 1; i <= resultSet.getMetaData().getColumnCount(); i++) {
                    columnNames.add(resultSet.getMetaData().getColumnLabel(i));
                }
                velocityContext.put("columnNames", columnNames);

                // Get the Column Names
                int rowCount = 0;
                while (resultSet.next()) {
                    rowCount++;
                    ArrayList<String> row = new ArrayList<String>();
                    for (int i = 1; i <= resultSet.getMetaData().getColumnCount(); i++) {
                        row.add(resultSet.getString(i));
                    }
                    dataRows.add(row);
                }

                Date entryDate = new Date(System.currentTimeMillis());

                // Did we pass the soft row limit?
                if (rowCount > softRowLimit) {

                    String message = "<p>The user <b>" + fullName + " ( " + userName + " )</b> executed an sql query\n "
                                    + "using the Database Connection Macro on the page <a href=\"" + pageUrl + "\">" + pageName + "</a>\n "
                                    + "in the space <b>" + spaceName + "</b>. It returned " + rowCount + " rows\n "
                                    + "exceeding the soft row limit of " + softRowLimit + ".</p>\n";

                    sendEmail(pluginConfigManager, "Confluence DB Connector Soft Row Limit Warning", message, null);
                }

                String logMessage = "DB Query( "
                                  + "fullName="+fullName+", "
                                  + "userName="+userName+", "
                                  + "pageName="+pageName+", "
                                  + "spaceName="+spaceName+", "
                                  + "databaseProfile="+profile.getName()+", "
                                  + "rowCount="+rowCount+", "
                                  + "queryDuration="+(endQuery - startQuery)+")\n";

                if( pluginConfigManager.getAtlassianLogLevel().equals( "Warn" ))
                {
                    log.warn( logMessage );
                }
                else
                {
                    log.info( logMessage );
                }

                ConnectionProfile auditLogProfile = pluginConfigManager.getConnectionProfileFromId( pluginConfigManager.getAuditLogDbProfile() );
                if( auditLogProfile != null )
                {
                   Runnable runnable = new InsertLogEntryIntoAuditDatabase( auditLogProfile,
                                                                            pluginConfigManager,
                                                                            profile,
                                                                            fullName,
                                                                            userName,
                                                                            pageUrl,
                                                                            pageName,
                                                                            spaceName,
                                                                            rowCount,
                                                                            (endQuery - startQuery),
                                                                            rawSql );
                    new Thread(runnable).start();
                }

                if (pluginConfigManager.getLogEmail() != null && !Objects.equals(pluginConfigManager.getLogEmail(), "")) {
                    String delimiter = "-----field-delimiter\n";
                    String keyValueSplitter = "-----key-value-splitter\n";
                    String message = null;

                    if( pluginConfigManager.getEmailContentFormat().equals( "XML" )) {
                        message = "<database-connector-log-entry>\n"
                                + "   <fullName><![CDATA[" + fullName + "]]></fullName>\n"
                                + "   <userName><![CDATA[" + userName + "]]></userName>\n"
                                + "   <pageName><![CDATA[" + pageName + "]]></pageName>\n"
                                + "   <pageUrl><![CDATA[" + pageUrl + "]]></pageUrl>\n"
                                + "   <spaceName><![CDATA[" + spaceName + "]]></spaceName>\n"
                                + "   <databaseProfile><![CDATA[" + profile.getName() + "]]></databaseProfile>\n"
                                + "   <rowCount><![CDATA[" + rowCount + "]]></rowCount>\n"
                                + "   <queryDuration><![CDATA[" + (endQuery - startQuery) + "]]></queryDuration>\n"
                                + "   <sqlQuery><![CDATA[" + rawSql + "]]></sqlQuery>\n"
                                + "   <timestamp><![CDATA[" + LocalDateTime.now() + "]]></timestamp>\n"
                                + "</database-connector-log-entry>\n";
                    } else if( pluginConfigManager.getEmailContentFormat().equals( "JSON" )) {
                        message = "{\"fullName\":\"" +StringEscapeUtils.escapeJson(fullName)+"\","
                                + "\"userName\":\"" +StringEscapeUtils.escapeJson(userName)+"\","
                                + "\"pageName\":\"" +StringEscapeUtils.escapeJson(pageName)+"\","
                                + "\"pageUrl\":\""  +StringEscapeUtils.escapeJson(pageUrl)+"\","
                                + "\"databaseProfile\":\""  +StringEscapeUtils.escapeJson(profile.getName())+"\","
                                + "\"spaceName\":\""+StringEscapeUtils.escapeJson(spaceName)+"\","
                                + "\"rowCount\":\"" +rowCount+"\","
                                + "\"queryDuration\":\""+(endQuery - startQuery)+"\","
                                + "\"sqlQuery\":\"" +StringEscapeUtils.escapeJson(StringEscapeUtils.escapeJson(rawSql))+"\","
                                + "\"timestamp\":\""     +LocalDateTime.now()+"\"}";
                    } else if( pluginConfigManager.getEmailContentFormat().equals( "Parsable Text" )) {
                        message = delimiter + "fullName\n" + keyValueSplitter + fullName + "\n"
                                + delimiter + "userName\n" + keyValueSplitter + userName + "\n"
                                + delimiter + "pageName\n" + keyValueSplitter + pageName + "\n"
                                + delimiter + "pageUrl\n" + keyValueSplitter + pageUrl + "\n"
                                + delimiter + "databaseProfile\n" + keyValueSplitter + profile.getName() + "\n"
                                + delimiter + "spaceName\n" + keyValueSplitter + spaceName + "\n"
                                + delimiter + "rowCount\n" + keyValueSplitter + rowCount + "\n"
                                + delimiter + "queryDuration\n" + keyValueSplitter + (endQuery - startQuery) + "\n"
                                + delimiter + "sqlQuery\n" + keyValueSplitter + rawSql + "\n"
                                + delimiter + "timestamp\n" + keyValueSplitter + LocalDateTime.now() + "\n";
                    } else {
                        message = "fullName = " + fullName + "\n"
                                + "userName = " + userName + "\n"
                                + "pageUrl = " + pageUrl + "\n"
                                + "pageName = " + pageName + "\n"
                                + "spaceName = " + spaceName + "\n"
                                + "databaseProfile = " + profile.getName() + "\n"
                                + "rowCount = " + rowCount + "\n"
                                + "queryDuration = " + (endQuery - startQuery) + "\n"
                                + "timestamp = " + LocalDateTime.now() + "\n"
                                + "sqlQuery\n"
                                + "--------\n"
                                + rawSql + "\n";
                    }

                    sendPlainTextEmail(pluginConfigManager, "DB Connector Log Entry: Returned " + rowCount + " rows in " + (endQuery - startQuery) + " seconds at " + LocalDateTime.now(), message, pluginConfigManager.getLogEmail());
                }

                velocityContext.put("dataRows", dataRows);
            } catch (Exception e) {
                throw new MacroExecutionException(e);
            } finally {
                close();
            }
        }

        StringBuilder codeViewOfBody = new StringBuilder();

        codeViewOfBody.append("<ac:structured-macro ac:name=\"code\" ac:schema-version=\"1\">");
        codeViewOfBody.append("<ac:parameter ac:name=\"language\">sql</ac:parameter>");
        codeViewOfBody.append("<ac:parameter ac:name=\"title\">SQL Query</ac:parameter>");
        if (this.parameters.containsKey(COLLAPSE_QUERY)) {
            codeViewOfBody.append("<ac:parameter ac:name=\"collapse\">true</ac:parameter>");
        }
        codeViewOfBody.append("   <ac:plain-text-body><![CDATA[");
        codeViewOfBody.append(rawSql);
        codeViewOfBody.append("]]></ac:plain-text-body>");
        codeViewOfBody.append("</ac:structured-macro>");

        String databaseQueryContainerCssClass = "";
        String boxCssClass = "";
        if( parameters.containsKey( BOUNDING_BOX )){
            databaseQueryContainerCssClass = "panel database-query-container";
            boxCssClass = "database-query-box database-query-top database-query-body-container database-query-bottom";
            velocityContext.put("boxCssClass", boxCssClass);
        } else {
            databaseQueryContainerCssClass = "database-query-container";
        }
        velocityContext.put("databaseQueryContainerCssClass", databaseQueryContainerCssClass);

        if( parameters.containsKey(REFRESH_BUTTON) && macroId != null ){
            velocityContext.put("showRefreshMacroId", "true");
            velocityContext.put("refreshMacroId", macroId);
        }

        if (!parameters.containsKey(HIDE_HEADER)) {
            velocityContext.put("showHeader", "true" );
        }

        velocityContext.put("showQuery", this.parameters.get(QUERY_ISVISIBLE));
        velocityContext.put("code", codeViewOfBody.toString());
        return velocityHelperService.getRenderedTemplate(template, velocityContext);
    }

    private void sendEmail(PluginConfigManager pluginConfigManager, String subject, String body, String customEmail) {
        sendEmailLowLevel( pluginConfigManager, subject, body, customEmail, MIME_TYPE_HTML);
    }

    private void sendPlainTextEmail(PluginConfigManager pluginConfigManager, String subject, String body, String customEmail) {
        sendEmailLowLevel( pluginConfigManager, subject, body, customEmail, MIME_TYPE_TEXT);
    }

    private void sendEmailLowLevel(PluginConfigManager pluginConfigManager, String subject, String body, String customEmail, String mimeType ) {
        // needed for Confluence Data Center as the config could have been
        // updated by an instance of Confluence other than this one.
        pluginConfigManager.loadFromStorage();

        if (customEmail == null || Objects.equals(customEmail, "")) {
            customEmail = pluginConfigManager.getNotificationEmail();
        }

        try{
            MailQueueItem mailQueueItem = new ConfluenceMailQueueItem(customEmail, subject, body, mimeType);
            mailService.sendEmail( mailQueueItem );
        } catch( Exception e ){
            System.out.println( "Failed to send email." );
            System.out.println( e.getMessage() );
        }
    }

    private void close() {
        try {
            if (resultSet != null) {
                resultSet.close();
            }
            if (statement != null) {
                statement.close();
            }
            if (connect != null) {
                connect.close();
            }
        } catch (Exception e) {

        }
    }

    public BodyType getBodyType() {
        return BodyType.PLAIN_TEXT;
    }

    public OutputType getOutputType() {
        return OutputType.BLOCK;
    }
}
